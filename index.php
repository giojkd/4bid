<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>4BID | Accommodation Business Guru | Revenue Management Expert</title>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/animate.css">
  <link rel="stylesheet" type="text/css" href="css/cubeportfolio.min.css">
  <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
  <link rel="stylesheet" type="text/css" href="css/swiper.min.css">
  <link rel="stylesheet" type="text/css" href="css/settings.css">
  <link rel="stylesheet" type="text/css" href="css/bootsnav.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/yellow.css">
  <link rel="icon" href="images/favicon.png">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 ANNA DEMOSTHENOUS and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>


<body id="home" data-spy="scroll" data-target=".navbar" data-offset="50">

  <!--Page Loader-->
  <div class="loader">
    <div class="spinner">
      <div class="dot1"></div>
      <div class="dot2"></div>
    </div>
  </div>
  <!--Page Loader ends-->


  <!--Header Starts-->
  <header class="default">
    <nav class="navbar navbar-default navbar-fixed bootsnav">
      <div class="container">
        <div class="menu-icon">
          <span></span>
        </div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
            <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand" href="index.html">
            <img src="files/logo.png" class="logo logo-display" alt="">
            <img src="files/logo.png" class="logo logo-scrolled" alt="">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-menu">
          <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
            <li class="active">
              <a href="#home" class="scroll">Home</a>
            </li>
            <li><a href="#features" class="scroll">Dove interveniamo</a></li>
            <li><a href="#work" class="scroll"> Porfolio</a></li>
            <li><a href="#pricing" class="scroll">Revenue Management</a></li>
            <li><a href="#developedapps" class="scroll">App Sviluppate</a></li>
            <li><a href="#contactus" class="scroll">Contattaci</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <!--Header ends-->


  <!--Full Menu-->
  <section class="overlay-menu">
    <div class="menu-icon active">
      <span></span>
    </div>
    <div class="centered">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 columns half text-center">
            <a href="index.html" class="logo-full heading_space"><img src="files/logo.png" alt=""></a>
          </div>
          <div class="col-sm-12 columns half text-left">
            <ul class="full-nav top20 heading_space">
              <li class="active">
                <a href="#home" class="scroll">Home</a>
              </li>
              <li><a href="#features" class="scroll">Dove interveniamo</a></li>
              <li><a href="#work" class="scroll"> Porfolio</a></li>
              <li><a href="#pricing" class="scroll">Revenue Management</a></li>

              <li><a href="#developedapps" class="scroll">App Sviluppate</a></li>
              <li><a href="#contactus" class="scroll">Contattaci</a></li>
            </ul>
          </div>
          <div class="col-sm-12">
            <ul class="social">
              <li><a href="https://it-it.facebook.com/4bidsrl/"><i class="fa fa-facebook"></i> </a> </li>



            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Full Menu-->

  <!--Main Slider-->
  <div id="revo_main_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
    <div id="maxo-main" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
      <ul>
        <!-- SLIDE  -->
        <li data-index="rs-17" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" class="text-center">
          <!-- MAIN IMAGE -->
          <img src="files/slide1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
          <!-- LAYER NR. 1 -->
          <div class="tp-caption tp-resizeme whitecolor text-center" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['227','227','227','227']" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
            <h1 class="text-captilize text-center">
              La nostra <strong>esperienza</strong> al tuo
              <span> servizio </span>
            </h1>
          </div>
          <!--
              <div class="tp-caption tp-resizeme text-center" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['390','390','360','360']" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="s:900;e:Power2.easeInOut;s:900;e:Power2.easeInOut;" data-start="2500" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                <a href="#" class="button light">Getting Started</a>
                <a href="#" class="button white">Learn more</a>
              </div>-->
        </li>
        <!-- SLIDE 2  -->
        <li data-index="rs-18" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500">
          <!-- MAIN IMAGE -->
          <img src="files/slide2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
          <!-- LAYER NR. 1 -->
          <div class="tp-caption tp-resizeme whitecolor" data-x="['left','left','left','left']" data-hoffset="['10','0','0','0']" data-y="['227','227','227','227']" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
            <h1 class="text-captilize">
              La garanzia di un <strong>Case History</strong>
              <span> di successo </span>
            </h1>
          </div>
          <!--
              <div class="tp-caption tp-resizeme" data-x="['left','left','left','left']" data-hoffset="['10','0','0','0']" data-y="['390','390','360','360']" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="s:900;e:Power2.easeInOut;s:900;e:Power2.easeInOut;" data-start="2500" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                <a href="#" class="button light">Getting Started</a>
                <a href="#" class="button white">Learn more</a>
              </div>-->
        </li>
        <!-- SLIDE 3  -->
        <li data-index="rs-19" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500" class="text-right">
          <!-- MAIN IMAGE -->
          <img src="files/slide3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
          <!-- LAYER NR. 1 -->

          <div class="tp-caption tp-resizeme whitecolor text-right" data-x="['right','right','right','right']" data-hoffset="['10','0','0','0']" data-y="['227','227','227','227']" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
            <h1 class="text-captilize">
              Il <strong>revenue management</strong> al tuo
              <span> servizio </span>
            </h1>
          </div>
          <!--
              <div class="tp-caption tp-resizeme" data-x="['right','right','right','right']" data-hoffset="['10','0','0','0']" data-y="['390','390','360','360']" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="s:900;e:Power2.easeInOut;s:900;e:Power2.easeInOut;" data-start="2500" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                <a href="#" class="button light">Getting Started</a>
                <a href="#" class="button white">Learn more</a>
              </div>-->
        </li>
      </ul>
    </div>
  </div>
  <!--Main Slider ends -->


  <!--About Us-->
  <section id="features" class="padding">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <p class="title bottom0">Il nostro campo d'azione </p>
          <h2 class="text-captalize darkcolor bottom30">Dove interveniamo</h2>
        </div>
        <div class="icon_wrap text-center clearfix">
          <div class="col-sm-4 top30">
            <div class="icon_box">
              <i class="fa fa-rocket"></i>
              <h4 class="text-capitalize bottom15">Mettiamo il turbo alla tua struttura</h4>
              <p class="no_bottom">Forniamo consulenza e/o management alle strutture ricettive sia in fase di start-up che nella riorganizzazione aziendale.<br><br></p>
            </div>
          </div>
          <div class="col-sm-4 top30">
            <div class="icon_box border_radius">
              <i class="fa fa-globe"></i>
              <h4 class="text-capitalize bottom15">Ampliamo la tua base commerciale</h4>
              <p class="no_bottom">La vasta esperienza maturata nell'accommodation business ci permette di configurare il miglior balance dei canali per la commercializzazione della tua struttura.</p>
            </div>
          </div>
          <div class="col-sm-4 top30">
            <div class="icon_box border_radius">
              <i class="fa fa-line-chart"></i>
              <h4 class="text-capitalize bottom15">Incrementa il fatturato del 45%</h4>
              <p class="no_bottom">L'incremento del fatturato medio generato dai nostri clienti grazie alle nostre consulenze è del 45%.<br><br></p>
            </div>
          </div>
          <div class="col-sm-4 top30">
            <div class="icon_box border_radius">
              <i class="fa fa-pencil-square-o"></i>
              <h4 class="text-capitalize bottom15">Head hunting &amp; Staff recruitment</h4>
              <p class="no_bottom">Ci occupiamo di reperire e formare il personale della tua struttura affinché affronti al meglio la stagione sfruttando le più innovative tecniche di vendita.</p>
            </div>
          </div>
          <div class="col-sm-4 top30">
            <div class="icon_box border_radius">
              <i class="fa fa-umbrella"></i>
              <h4 class="text-capitalize bottom15">Sempre al tuo fianco</h4>
              <p class="no_bottom">Ti affianchiamo in loco per 15 gg nel periodo a cavallo fra pre-apertura ed apertura della struttura.<br><br></p>
            </div>
          </div>
          <div class="col-sm-4 top30">
            <div class="icon_box border_radius">
              <i class="fa fa-laptop"></i>
              <h4 class="text-capitalize bottom15">Curiamo la tua immagine sul web</h4>
              <p class="no_bottom">Realizziamo il tuo sito internet e curiamo i social coordinatamente alla strategia commerciale che definiamo per te.<br><br></p>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!--About Us ends -->


  <!--Testimonials-->
  <!--
        <section class="bg bglight" id="maxo-testimonials">
        <div class="bghalf imgone hidden-xs"></div>
        <div class="container">
        <div class="row">
        <div class="col-md-offset-6 col-md-6 col-sm-12">
        <div class="bglight padding darkcolor right">
        <h2 class="text-captalize heading_space">Maxo is Creative</h2>
        <div class="testimonial">
        <div class="testimonial_slide">
        <span class="photo bottom25"><img src="images/testimonial1.png" alt=""> </span>
        <div class="signature bottom25">
        <h6>Sandra Wilson</h6>
        <span>AB DESIGN STUDIOS</span>
      </div>
      <p class="bottom0 top15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lobortis eget nibh et lobortis. Nulla in odio quis augue ultrices blandit. Phasellus ipsum nibh, porta non sapien non, efficitur pulvinar neque. Pellentesque aliquam, massa eu fringilla egestas, </p>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section>
-->
  <!--Testimonials ends-->


  <!--Gallery-->
  <!--<section id="work" class="padding">
<div class="container">
<div class="row">
<div class="col-sm-7">
<p class="bottom0">Lorem ipsum sit</p>
<h2 class="text-captalize darkcolor heading_space">Portfolio</h2>
</div>
<div class="col-sm-5 text-right">
<div id="work-filters" class="cbp-l-filters heading_space">
<div data-filter="*" class="cbp-filter-item">
<span>graphics</span>
</div>
<div data-filter=".web" class="cbp-filter-item">
<span>web</span>
</div>
<div data-filter=".print" class="cbp-filter-item">
<span>Print</span>
</div>
<div data-filter=".logo" class="cbp-filter-item">
<span>Logo</span>
</div>
</div>
</div>
</div>
<div id="work-gallery" class="cbp">
<div class="cbp-item logo">
<a href="images/portfolio1.jpg" data-fancybox="gallery">
<img src="images/portfolio1.jpg" alt="">
</a>
<div class="overlay text-uppercase whitecolor">
<a class="likeus" href="javascript:void(0)"><span><i class="fa fa-thumbs-up"></i> 25</span> </a>
<a href="images/portfolio1.jpg" data-fancybox="gallery">
<h4>ANNA DEMOSTHENOUS</h4>
<p class="heading">PHOTOGRAPHY <span class="colorbar"></span></p>
</a>
</div>
</div>
<div class="cbp-item logo web print">
<a href="images/portfolio2.jpg" data-fancybox="gallery">
<img src="images/portfolio2.jpg" alt="">
</a>
<div class="overlay text-uppercase whitecolor">
<a class="likeus" href="javascript:void(0)"><span><i class="fa fa-thumbs-up"></i> 25</span> </a>
<a href="images/portfolio2.jpg" data-fancybox="gallery">
<h4>ANNA DEMOSTHENOUS</h4>
<p class="heading">PHOTOGRAPHY <span class="colorbar"></span></p>
</a>
</div>
</div>
<div class="cbp-item print graphics logo">
<a href="images/portfolio3.jpg" data-fancybox="gallery">
<img src="images/portfolio3.jpg" alt="">
</a>
<div class="overlay text-uppercase whitecolor">
<a class="likeus" href="javascript:void(0)"><span><i class="fa fa-thumbs-up"></i> 25</span> </a>
<a href="images/portfolio3.jpg" data-fancybox="gallery">
<h4>ANNA DEMOSTHENOUS</h4>
<p class="heading">PHOTOGRAPHY <span class="colorbar"></span></p>
</a>
</div>
</div>
<div class="cbp-item graphics web print">
<a href="images/portfolio4.jpg" data-fancybox="gallery">
<img src="images/portfolio4.jpg" alt="">
</a>
<div class="overlay text-uppercase whitecolor">
<a class="likeus" href="javascript:void(0)"><span><i class="fa fa-thumbs-up"></i> 25</span> </a>
<a href="images/portfolio4.jpg" data-fancybox="gallery">
<h4>ANNA DEMOSTHENOUS</h4>
<p class="heading">PHOTOGRAPHY <span class="colorbar"></span></p>
</a>
</div>
</div>
<div class="cbp-item logo print">
<a href="images/portfolio5.jpg" data-fancybox="gallery">
<img src="images/portfolio5.jpg" alt="">
</a>
<div class="overlay text-uppercase whitecolor">
<a class="likeus" href="javascript:void(0)"><span><i class="fa fa-thumbs-up"></i> 25</span> </a>
<a href="images/portfolio5.jpg" data-fancybox="gallery">
<h4>ANNA DEMOSTHENOUS</h4>
<p class="heading">PHOTOGRAPHY <span class="colorbar"></span></p>
</a>
</div>
</div>
<div class="cbp-item">
<img src="images/portfolio6.jpg" alt="">
<div class="text_wrap bg_transparent text-capitalize">
<h5 class="bottom5 top20 darkcolor">Projects by</h5>
<h2 class="bottom5 darkcolor">Maxo 07</h2>
<h5 class="darkcolor">For our amazing clients</h5>
<a href="javascript:void(0)" class="button light top30">All Projects</a>
</div>
</div>
</div>
</div>
</section>-->
  <!--Gallery ends-->


  <!--Parallax-->
  <section id="m-parallax" class="parallax-bg parallax padding" style="background-image:url('files/parallax1.jpg')">
    <div class="container text-center">
      <div class="row">
        <div class="col-sm-1 col-md-2"></div>
        <div class="col-sm-10 col-md-8">
          <h2 class="whitecolor"><i class="fa fa-quote-left"></i> Revenue Management <i class="fa fa-quote-right"></i></h2>
          <div class="testimonial center">
            <div class="testimonial_slide">
              <span class="photo bottom25"><img src="files/filippo.jpg" alt=""> </span>
              <div class="signature bottom25" style="color:white">
                <h6>Filippo Mancini</h6>
                <span>CEO 4BID srl, CEO I Barronci srl</span>
              </div>

            </div>
          </div>
          <p class="top20" style="color:white">- Il revenue management è la soluzione più efficace per garantire costante occupazione e massima produttività alle aziende turistico ricettive. -</p>
        </div>
        <div class="col-sm-1 col-md-2"></div>
      </div>
    </div>
  </section>
  <!--Parallax Ends-->

  <section id="work" class="padding">
    <div class="container">

      <div class="row">
        <div class="col-sm-7">
          <p class="bottom0">Alcuni dei success cases raccolti nel portfolio . Come si può vedere dai numeri, lincremento medio è notevole, e si raggiunge il massimo dell'efficienza dopo 3 anni di consulenza.</p>
          <h2 class="text-captalize darkcolor heading_space">Portfolio</h2>
        </div>
      </div>
      <div class="row">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Tipologia</th>
              <th>Location</th>
              <th>Rooms</th>
              <th>Revenues before consulting (K€)</th>
              <th>Revenues after consulting (K€) 1st year</th>
              <th>Revenues after consulting (K€) 2nd year</th>
              <th>Revenues increase (%)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Resort 4*</td>
              <td>Chianti</td>
              <td>21</td>
              <td>550</td>
              <td>700</td>
              <td>930</td>
              <td>75,27</td>
            </tr>
            <tr>
              <td>Agriturismo</td>
              <td>Chianti</td>
              <td>18</td>
              <td>200</td>
              <td>350</td>
              <td>420</td>
              <td>83,33</td>
            </tr>
            <tr>
              <td>Dimora Storica</td>
              <td>Firenze</td>
              <td>6</td>
              <td>50</td>
              <td>130</td>
              <td>180</td>
              <td>72,22</td>
            </tr>
            <tr>
              <td>Agriturismo</td>
              <td>Chianti</td>
              <td>18 &gt; 33</td>
              <td>330</td>
              <td>650</td>
              <td>1150</td>
              <td>56,52</td>
            </tr>
            <tr>
              <td>Agriturismo</td>
              <td>Val d'Elsa</td>
              <td>16</td>
              <td>100</td>
              <td>280</td>
              <td>330</td>
              <td>84,85</td>
            </tr>
            <tr>
              <td>Hotel 3*</td>
              <td>Firenze</td>
              <td>17</td>
              <td>300</td>
              <td>630</td>
              <td>780</td>
              <td>80,77</td>
            </tr>
            <tr>
              <td>Hotel 3*</td>
              <td>Cecina</td>
              <td>60</td>
              <td>560</td>
              <td>820</td>
              <td>910</td>
              <td>90,11</td>
            </tr>
            <tr>
              <td>Aparthotel</td>
              <td>Pelago</td>
              <td>28</td>
              <td>Startup</td>
              <td>390</td>
              <td>450</td>
              <td>86,67</td>
            </tr>
            <tr>
              <td>Hotel 4*L</td>
              <td>Bellaria</td>
              <td>42</td>
              <td>1200</td>
              <td>1800</td>
              <td>2100</td>
              <td>85,71</td>
            </tr>
            <tr>
              <td>Hotel 4*</td>
              <td>Siena</td>
              <td>28</td>
              <td>420</td>
              <td>625</td>
              <td>660</td>
              <td>94,70
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </section>


  <!--Parallax-->
  <section id="m-parallax" class="parallax-bg parallax padding" style="background-image:url('files/parallax2.jpg')">
    <div class="container text-center">
      <div class="row">
        <div class="col-sm-1 col-md-2"></div>
        <div class="col-sm-10 col-md-8">
          <h2 class="whitecolor"><i class="fa fa-quote-left"></i> Great things in business are never done by one person. They're done by a team of people. <i class="fa fa-quote-right"></i></h2>

          <p class="top20">- Steve Jobs -</p>
        </div>
        <div class="col-sm-1 col-md-2"></div>
      </div>
    </div>
  </section>
  <!--Parallax Ends-->

  <section id="pricing" class="padding">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p class="title bottom0">Tariffazione in tempo reale</p>
          <h2 class="text-capitalize darkcolor bottom20">Revenue Management</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-center">
          <p class="text-justify">
            Per gestione dei ricavi (dall'inglese: yield management o revenue management, a volte tradotto con gestione della redditività, tariffazione in tempo reale) si intende il sistema di gestione delle capacità disponibili (camere d'albergo, posti a sedere nel trasporto aereo, biglietti di teatri, cinema, posti al ristorante, ecc ) che ha come obiettivo la massimizzazione e l'ottimizzazione del volume di affari.
          </p>
          <p>
          </p>
          <p class="text-justify">
            La tecnica di vendita che ne deriva consente di modificare i ricavi per unità sulla base del reale andamento della domanda, realizzando i maggiori ricavi e il più alto tasso di vendite possibile in ogni periodo dell'anno. Una delle manifestazioni più evidenti è l'applicazione di prezzi più alti quando la domanda è elevata e prezzi scontati quando è più bassa.
          </p>



        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-6 text-left">
          <h3 cass="darkcolor">Condizioni di applicabilità del RM</h3>
          <p>L'ambito di applicazione delle tecniche della gestione dei ricavi riguarda attività:</p>
          <ol>
            <li>A capacità fissa e limitata, dove i costi fissi e i costi per incrementare la capacità sono elevati</li>
            <li>Prenotazione/vendita anticipate rispetto al consumo</li>
            <li>Domanda variabile e non perfettamente prevedibile</li>
            <li>Presenza di un prodotto/servizio deperibile, per cui mancare una vendita in un giorno significa una perdita non recuperabile</li>
            <li>Prodotti/servizi indifferenziati tra clienti con comportamenti d'acquisto (preferenze, funzione di utilità) diversi: lo stesso prodotto/servizio può essere venduto a clienti differenti</li>
            <li>Possibilità di segmentare la domanda. L'ipotesi precedente vale quindi per sottoinsiemi del mercato, i segmenti individuabili al suo interno</li>
            <li>Sistema informativo accurato e dettagliato</li>
          </ol>
        </div>
        <div class="col-sm-6 text-right">
          <h3>Settori di applicazione del RM</h3>
          <p>Gli ambiti di applicazione riguardano principalmente il settore dei servizi</p>
          <ol>
            <li>Industria dei divertimenti (cinema, teatri, musei)</li>
            <li>Settore dei trasporti (aereo, navale, ferroviario, via camion, agenzie di noleggio auto)</li>
            <li>Alberghi, b&amp;b;, agriturismi, resort, catene di alberghi e villaggi turistici, etc...</li>
            <li>Vendita al dettaglio (bar, supermercati, ristoranti, pizzerie)</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!--PRICINGS Starts-->
  <?php
  /*
    <section id="pricing" class="padding">
    <div class="container text-center">
    <div class="row">
    <div class="col-sm-12">
    <p class="title bottom0">Lorem ipsum sit </p>
    <h2 class="text-capitalize darkcolor bottom20">Packages</h2>
    </div>
    </div>
    <div class="row">
    <div class="col-sm-4">
    <div class="pricing_item top40">
    <div class="pricebox bottom25 clearfix">
    <div class="price_title">
    <h3 class="darkcolor text-uppercase bottom10"> Basic</h3>
    <span class="ratings">
    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
    <i class="fa fa-star-half-full"></i>
    <i class="fa fa-star-o"></i>
    <i class="fa fa-star-o"></i>
    </span>
    </div>
    <div class="price darkcolor">
    <p><strong>$19</strong><span class="month">/mo</span></p>
    </div>
    </div>
    <p class="availability">5GB Desk Space</p>
    <p class="availability">Unlimited Bandwidth</p>
    <p class="availability">Weekly Backups</p>
    <p class="availability">Powerful Admin Panel</p>
    <p class="availability">100 Email Accounts</p>
    <p class="availability">8 Free Forks Every Months</p>
    <p class="availability">Free Software Installer</p>
    <a href="javascript:void(0)" class="button dark top20">GET IT NOW</a>
    </div>
    </div>
    <div class="col-sm-4">
    <div class="pricing_item active top40">
    <div class="pricebox bottom25 clearfix">
    <div class="price_title">
    <h3 class="darkcolor text-uppercase bottom10"> pro</h3>
    <span class="ratings">
    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
    <i class="fa fa-star"></i>
    <i class="fa fa-star"></i>
    <i class="fa fa-star-o"></i>
    </span>
    </div>
    <div class="price darkcolor">
    <p><strong>$49</strong><span class="month">/mo</span></p>
    </div>
    </div>
    <p class="availability">5GB Desk Space</p>
    <p class="availability">Unlimited Bandwidth</p>
    <p class="availability">Weekly Backups</p>
    <p class="availability">Powerful Admin Panel</p>
    <p class="availability">100 Email Accounts</p>
    <p class="availability">8 Free Forks Every Months</p>
    <p class="availability">Free Software Installer</p>
    <a href="javascript:void(0)" class="button white top20">GET IT NOW</a>
    </div>
    </div>
    <div class="col-sm-4">
    <div class="pricing_item top40">
    <div class="pricebox bottom25 clearfix">
    <div class="price_title">
    <h3 class="darkcolor text-uppercase bottom10"> Premium</h3>
    <span class="ratings">
    <i class="fa fa-star"></i> <i class="fa fa-star"></i>
    <i class="fa fa-star"></i>
    <i class="fa fa-star"></i>
    <i class="fa fa-star"></i>
    </span>
    </div>
    <div class="price darkcolor">
    <p><strong>$99</strong><span class="month">/mo</span></p>
    </div>
    </div>
    <p class="availability">5GB Desk Space</p>
    <p class="availability">Unlimited Bandwidth</p>
    <p class="availability">Weekly Backups</p>
    <p class="availability">Powerful Admin Panel</p>
    <p class="availability">100 Email Accounts</p>
    <p class="availability">8 Free Forks Every Months</p>
    <p class="availability">Free Software Installer</p>
    <a href="javascript:void(0)" class="button dark top20">Customize</a>
    </div>
    </div>
    </div>
    </div>
    </section>
    */
  ?>
  <!--PRICINGS ends-->


  <!--Latest Updates-->
  <?php
  /*
    <section class="bg bglight" id="maxo_news">
    <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-12">
    <div class="padding darkcolor equalheight left">
    <h2 class="text-capitalize heading_space">latest updates</h2>
    <div class="updates">
    <div class="update_slide">
    <div class="latest_updates">
    <div class="updatephoto">
    <img src="images/update1.jpg" alt="">
    <a href="javascript:void(0)" class="ivydatedate">19 apr</a>
    </div>
    <div class="update_text">
    <h6 class="bottom20"><a href="blog-detail.html">Capitalize on hanging fruit</a> </h6>
    <p class="bottom40">Bring to the table win-win survival strategies toensure proactive domination. At the end of the day. giving forward a new normal that has</p>
    </div>
    </div>
    <div class="latest_updates bottom25">
    <div class="updatephoto">
    <img src="images/update2.jpg" alt="">
    <a href="javascript:void(0)" class="ivydatedate">19 feb</a>
    </div>
    <div class="update_text">
    <h6 class="bottom20"><a href="blog-detail.html">Capitalize on hanging fruit</a></h6>
    <p>Bring to the table win-win survival strategies toensure proactive domination. At the end of the day. giving forward a new normal that has </p>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="bghalf imgtwo equalheight hidden-xs"></div>
    </section>
    */
  ?>
  <!--Latest Updates ends-->


  <!-- Our Team-->
  <?php
  /*
    <section class="padding" id="team">
    <div class="container ivy-team">
    <div class="row">
    <div class="col-sm-12 text-center">
    <p class="title bottom0">Lorem ipsum sit </p>
    <h2 class="text-capitalize heading_space darkcolor">Our team</h2>
    </div>
    <div class="col-sm-12">
    <div class="ivy-team">
    <div class="swiper-container">
    <div class="swiper-wrapper">
    <!-- Slides -->
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team1.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">Emma Watson</h3>
    <p class="darkcolor">Managing director </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team2.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">James Cameron</h3>
    <p class="darkcolor">Chief technology officer </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team3.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">sarah wilson</h3>
    <p class="darkcolor">Graphis Designer </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team4.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">David Willey</h3>
    <p class="darkcolor">marketing manager </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team1.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">Emma Watson</h3>
    <p class="darkcolor">Managing director </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team2.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">James Cameron</h3>
    <p class="darkcolor">Chief technology officer </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team3.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">sarah wilson</h3>
    <p class="darkcolor">Graphis Designer </p>
    </div>
    </div>
    <div class="swiper-slide ourteam">
    <div class="image">
    <ul class="social_vertical">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
    <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> </a> </li>
    </ul>
    <img src="images/team4.jpg" alt="">
    </div>
    <div class="team_caption darkcolor">
    <div class="overlay"></div>
    <h3 class="bottom10">David Willey</h3>
    <p class="darkcolor">marketing manager </p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="text-center">
    <div class="swiper-pagination top40"></div>
    </div>
    </div>
    </div>
    </div>
    </section>
    */
  ?>
  <!--Our Team ends-->


  <!--Blog-->
  <?php
  /*
    <section id="maxo-blog" class="padding bglight darkcolor">
    <div class="container">
    <div class="row">
    <div class="col-sm-12 text-center">
    <p class="title bottom0">Lorem ipsum sit </p>
    <h2 class="text-capitalize heading_space">our blog</h2>
    <div class="our-blog">
    <div class="swiper-container">
    <div class="swiper-wrapper">
    <!-- Slides -->
    <div class="swiper-slide">
    <div class="ourblog">
    <div class="row">
    <div class="col-sm-12">
    <div class="image">
    <a href="blog.html"><img src="images/blog1.jpg" alt="blog"></a>
    <div class="desc_tags">
    <a href="#" class="ivydatedate">19 apr</a>
    <a href="#" class="ivydatedate">ADMIN</a>
    <a href="#" class="ivydatedate">DESIGN</a>
    </div>
    </div>
    </div>
    <div class="col-sm-12">
    <div class="blog_desc">
    <h3 class="top25 bottom20"><a href="blog.html">Capitalize on hanging fruit</a> </h3>
    <p class="bottom20">Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has in real-time will.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="swiper-slide">
    <div class="ourblog">
    <div class="row">
    <div class="col-sm-12">
    <div class="image">
    <a href="blog.html"><img src="images/blog2.jpg" alt="blog"></a>
    <div class="desc_tags">
    <a href="#" class="ivydatedate">19 apr</a>
    <a href="#" class="ivydatedate">ADMIN</a>
    <a href="#" class="ivydatedate">DESIGN</a>
    </div>
    </div>

    </div>
    <div class="col-sm-12">
    <div class="blog_desc">
    <h3 class="top25 bottom20"><a href="blog.html">Capitalize on hanging fruit</a> </h3>
    <p class="bottom20">Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has in real-time will.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="swiper-slide">
    <div class="ourblog">
    <div class="row">
    <div class="col-sm-12">
    <div class="image">
    <a href="blog.html"><img src="images/blog3.jpg" alt="blog"></a>
    <div class="desc_tags">
    <a href="#" class="ivydatedate">19 apr</a>
    <a href="#" class="ivydatedate">ADMIN</a>
    <a href="#" class="ivydatedate">DESIGN</a>
    </div>
    </div>
    </div>
    <div class="col-sm-12">
    <div class="blog_desc">
    <h3 class="top25 bottom20"><a href="blog.html">Capitalize on hanging fruit </a></h3>
    <p class="bottom20">Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has in real-time will.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="swiper-slide">
    <div class="ourblog">
    <div class="row">
    <div class="col-sm-12">
    <div class="image">
    <a href="blog.html"><img src="images/blog1.jpg" alt="blog"></a>
    <div class="desc_tags">
    <a href="#" class="ivydatedate">19 apr</a>
    <a href="#" class="ivydatedate">ADMIN</a>
    <a href="#" class="ivydatedate">DESIGN</a>
    </div>
    </div>
    </div>
    <div class="col-sm-12">
    <div class="blog_desc">
    <h3 class="top25 bottom20"><a href="blog.html">Capitalize on hanging fruit</a> </h3>
    <p class="bottom20">Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has in real-time will.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="swiper-slide">
    <div class="ourblog">
    <div class="row">
    <div class="col-sm-12">
    <div class="image">
    <a href="blog.html"><img src="images/blog2.jpg" alt="blog"></a>
    <div class="desc_tags">
    <a href="#" class="ivydatedate">19 apr</a>
    <a href="#" class="ivydatedate">ADMIN</a>
    <a href="#" class="ivydatedate">DESIGN</a>
    </div>
    </div>

    </div>
    <div class="col-sm-12">
    <div class="blog_desc">
    <h3 class="top25 bottom20"><a href="blog.html">Capitalize on hanging fruit</a> </h3>
    <p class="bottom20">Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has in real-time will.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="swiper-slide">
    <div class="ourblog">
    <div class="row">
    <div class="col-sm-12">
    <div class="image">
    <a href="blog.html"><img src="images/blog3.jpg" alt="blog"></a>
    <div class="desc_tags">
    <a href="#" class="ivydatedate">19 apr</a>
    <a href="#" class="ivydatedate">ADMIN</a>
    <a href="#" class="ivydatedate">DESIGN</a>
    </div>
    </div>
    </div>
    <div class="col-sm-12">
    <div class="blog_desc">
    <h3 class="top25 bottom20"><a href="blog.html">Capitalize on hanging fruit</a> </h3>
    <p class="bottom20">Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has in real-time will.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="swiper-pagination top20"></div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </section>
    */
  ?>
  <!--Blog ends -->


  <!--We Work With....-->
  <?php
  /*
    <div id="we-work" class=" padding">
    <div class="container">
    <div class="col-md-12 col-sm-12 text-center">
    <p class="title bottom0">Lorem ipsum sit </p>
    <h2 class="text-capitalize darkcolor heading_space">we work with</h2>
    </div>
    <div class="col-md-12 col-sm-12">
    <div class="partners">
    <div class="swiper-container">
    <div class="swiper-wrapper">
    <!-- Slides -->
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo1.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo2.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo3.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo4.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo5.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo6.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo7.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo8.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo9.jpg" alt="we work">
    </div>
    </div>
    <div class="swiper-slide">
    <div class="image">
    <img src="images/logo10.jpg" alt="we work">
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    */
  ?>
  <!--We Work With ends....-->

  <section class="bg bglight" id="developedapps">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="padding equalheight darkcolor left">
            <h2 class="text-center text-capitalize bottom25">Da Tiberio a San Casciano</h2>
          </div>
        </div>
        <div class="col-sm-6 text-center">
          <img src="/images/da-tiberio-app-hand.png" alt="">
        </div>
      </div>
    </div>
  </section>


  <!--CONTACT Us starts-->
  <section class="bg " id="contactus">
    <div class="bghalf equalheight">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2886.3524924760004!2d11.165726815726668!3d43.661638359902724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a4f8210a99545%3A0xb5a8a06ef3d58919!2sVia%20Sorripa%2C%2010%2C%2050026%20San%20Casciano%20in%20Val%20di%20pesa%20FI!5e0!3m2!1sen!2sit!4v1585597587328!5m2!1sen!2sit" style="border:0; width: 100%; height: 100%" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="padding equalheight darkcolor left">
            <h2 class="text-capitalize bottom25">Contattaci</h2>
            <div class="locations top15">
              <i class="fa fa-phone"></i>
              <span><a href="tel:+390558290741">+(39) 055 829 07 41</a></span>
            </div>
            <div class="locations top15">
              <i class="fa fa-envelope-o"></i>
              <span><a href="mailto:info@4bid.it">info@4bid.it</a></span>
            </div>
            <div class="locations top15">
              <i class="fa fa-map-marker"></i>
              <span>Via Sorripa, 10 - 50026 San Casciano in Val di Pesa (Firenze)</span>
            </div>

            <!-- <form class="callus top40" onSubmit="return false">
                <div class="row">
                  <div class="col-sm-12 center"><div id="result"></div> </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <input type="text" class="form-control" placeholder="Nome e Cognome*" name="name" id="name" required="">
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="email" class="form-control" placeholder="Indirizzo Email*" name="email" id="email" required="">
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="tel" class="form-control" placeholder="Numero di Telefono" name="phone" id="phone">
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="text" class="form-control" placeholder="Sito internet della struttura ricettiva*" name="subject" id="subject" required="">
                  </div>
                  <hr>

                  <div class="col-sm-12 form-group">
                    <textarea class="form-control" placeholder="Il Suo Messaggio*" name="message" id="message" required=""></textarea>
                  </div>
                  <div class="col-sm-12 form-group">
                    <p class="helpblock">Compilando anche i seguenti campi riceverai in 72h una macro analisi gratuita sulla tua struttura ricettiva.</p>
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="text" class="form-control" placeholder="Ruolo ricoperto nella struttura" name="role" id="role">
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="text" class="form-control" placeholder="Numero di sistemazioni" name="accommodation_number" id="accommodation_number">
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="text" class="form-control" placeholder="Tipo di sistemazioni (camere, appartamenti, etc...)" name="accommodation_type" id="accommodation_type">
                  </div>
                  <div class="col-sm-6 form-group">
                    <input type="text" class="form-control" placeholder="Fatturato annuo in € netto iva (solo affitto camere)" name="net_yearly_revenue_from_rent" id="net_yearly_revenue_from_rent">
                  </div>
                  <div class="col-sm-12 form-group">
                    <p class="help-block">Inviando questo modulo si acconsente il trattamento dei dati personali sopra inseriti, per la fruizione del servizio richiesto, i dati saranno tutelati in base al D.L. n. 196 del 30 giugno 2003</p>
                  </div>
                  <div class="col-sm-12 form-group">
                    <button class="button dark top20" id="submit_btn">Invia</button>
                  </div>

                </div>
              </form> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- CONTACT Us ends-->


  <!--Footer starts-->
  <footer id="evy-footer" class="padding_half">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <p class="top10">Copyright <?= date('Y') ?>, All rights reserved.</p>
        </div>
        <div class="col-sm-6 text-right">
          <ul class="social top10">
            <li><a href="https://it-it.facebook.com/4bidsrl/"><i class="fa fa-facebook"></i> </a> </li>

          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!--Footer enda-->



  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-2.2.3.js"></script>


  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootsnav.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/jquery.cubeportfolio.min.js"></script>
  <script src="js/jquery.matchHeight-min.js"></script>
  <script src="js/jquery.fancybox.min.js"></script>
  <script src="js/jquery.parallax-1.1.3.js"></script>

  <script src="js/jquery.themepunch.tools.min.js"></script>
  <script src="js/jquery.themepunch.revolution.min.js"></script>
  <script src="js/revolution.extension.actions.min.js"></script>
  <script src="js/revolution.extension.layeranimation.min.js"></script>
  <script src="js/revolution.extension.navigation.min.js"></script>
  <script src="js/revolution.extension.parallax.min.js"></script>
  <script src="js/revolution.extension.slideanims.min.js"></script>
  <script src="js/revolution.extension.kenburn.min.js"></script>
  <script src="js/revolution.extension.video.min.js"></script>
  <script src="js/functions.js"></script>

</body>

</html>